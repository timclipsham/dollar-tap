require 'test/unit'
class MasterpassDataTest < Test::Unit::TestCase
  def setup
    @data = MasterpassData.new("config.yml")
  end
  
  def test_data_init
    assert @data != nil, "test init"
    assert @data.callback_domain == "http://projectabc.com", "test_callback_domain"
    assert @data.callback_path == "/oauthcallback", "test callback_path"
  end
  
  
end