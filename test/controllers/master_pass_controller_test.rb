require 'test_helper'

class MasterPassControllerTest < ActionController::TestCase
  
  def setup
    
  end

  test "get index" do
    get "index"
    assert_response :success, "response is successful"
    assert_not_nil session['data'], "session has data"
    assert_not_nil assigns :data
  end

  test "post oauth" do
    post "oauth", {
                  :xmlVersionDropdown => "v6",
                  :acceptedCardsCheckbox => ["amex", "master", "visa", "discover"],
                  :rewardsDropdown => false,
                  :authenticationCheckBox => false,
                  :shippingProfileDropdown => "customCard"
                  }
    assert_response :success, "oauth post is successful"
    assert_not_nil session['data'], "oauth session has data"
    assert_not_nil assigns :data
    data = session['data']
    assert_not_nil data.request_token
    assert_not_nil data.auth_header
    assert_not_nil data.encoded_auth_header
  end

  test "post oauthshoppingcart" do
    post "oauth", {
                  :xmlVersionDropdown => "v6",
                  :acceptedCardsCheckbox => ["amex", "master", "visa", "discover"],
                  :rewardsDropdown => false,
                  :authenticationCheckBox => false,
                  :shippingProfileDropdown => "customCard"
                  }
    post "oauthshoppingcart"
    data = session['data']
    assert_not_nil data.shopping_cart_response, "assigns shopping cart response"
  end

  test "handle oauthcallback" do
    get "oauthcallback",  {
                          :oauth_token => "oauth_token",
                          :oauth_verifier => "oauth_verifier",
                          :checkout_resource_url => "checkout_resource_url",
                          }
    assert_response :success, "response is successful"
    assert_not_nil session['data']
    assert_not_nil assigns :data
    data = session['data']
    assert data.request_token == "oauth_token", "assigns oauth token"
    assert data.verifier == "oauth_verifier", "assigns oauth verifier"
    assert data.checkout_resource_url == "checkout_resource_url", "assigns checkout resource url"
    begin
      get "oauthcallback",  {
                            :pairing_token => "pairing_token",
                            :pairing_verifier => "pairing_verifier"
                             }
    rescue Exception => e
      assert data.pairing_token == "pairing_token", "assigns pairing token"
      assert data.pairing_verifier == "pairing_verifier", "assigns pairing verifier"
    end
  end

  test "get access_token" do
    post "oauth", {
                  :xmlVersionDropdown => "v6",
                  :acceptedCardsCheckbox => ["amex", "master", "visa", "discover"],
                  :rewardsDropdown => false,
                  :authenticationCheckBox => false,
                  :shippingProfileDropdown => "customCard"
                  }
    get "oauthcallback",  {
                          :oauth_token => "oauth_token",
                          :oauth_verifier => "oauth_verifier",
                          :checkout_resource_url => "checkout_resource_url",
                          }

    begin
      post "accesstoken"
    rescue Exception => e
    end

  end

  test "send postback" do
    data = MasterpassData.new
    data.checkout = Checkout.new
    data.checkout.transactionId = "7025669"
    data.shopping_cart = ShoppingCart.new
    data.shopping_cart.subtotal = 500
    session['data'] = data
    post "oauthpostback"

    data = session['data']
    assert_not_nil data.merchant_transactions
    assert_not_nil data.post_transaction_sent_xml
    assert_not_nil data.post_transaction_received_xml
    merch_trans = data.merchant_transactions
    assert data.merchant_transactions[0].transactionId == "7025669", "merchant transactions transaction Id is set"
  end

  test "get request token" do
    post "pairingtoken"
    assert_not_nil session['data'].pairing_token
  end
  
  test "post merchant init" do
    post "pairingtoken"
    post "merchantinitialization"
    data = session['data']
    assert_not_nil data.merchant_init_request
    assert_not_nil data.merchant_init_response
  end
  
  test "pairing callback" do
    post :pairingconfiguration, {:dataTypes => "CARD,ADDRESS"}
    assert session['data'].pairing_data_types.length == 2, "data types should be added to data"
  end
  
end
