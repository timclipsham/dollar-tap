class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.decimal :amount
      t.references :charity, index: true

      t.timestamps
    end
  end
end
