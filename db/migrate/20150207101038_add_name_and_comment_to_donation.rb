class AddNameAndCommentToDonation < ActiveRecord::Migration
  def change
    add_column :donations, :name, :string
    add_column :donations, :comment, :text
  end
end
