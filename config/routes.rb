MastercardMasterpassSampleApplication::Application.routes.draw do
  resources :donations

  resources :charities

  root "charities#index"

  get "master_pass" => "master_pass#index"
  get "donations/:handle/new" => "donations#new"
  post "donations/:handle/pay" => "donations#pay"
  post "donations/:handle/cc" => "donations#cc"

  post "oauth" => 'master_pass#oauth'
  post "oauthshoppingcart" => 'master_pass#oauthshoppingcart'

  get "oauthcallback" => 'donations#oauthcallback'
  # get "oauthcallback" => 'master_pass#oauthcallback'

  post "oauthcheckout" => 'master_pass#oauthcheckout'
  post "accesstoken" => 'master_pass#accesstoken'
  post "pairingtoken" => 'master_pass#pairingtoken'
  post "merchantinitialization" => 'master_pass#merchantinitialization'
  post "pairingconfiguration" => 'master_pass#pairingconfiguration'
  get "pairingcallback" => 'master_pass#pairingcallback'
  post "precheckout" => 'master_pass#precheckout'
  post "oauthpostback" => 'master_pass#oauthpostback'
  post "expresscheckout" => 'master_pass#expresscheckout'
  get "expresscheckout" => 'master_pass#expresscheckout'
  post "cart" => 'master_pass#cart'
  get "cartcallback" => 'master_pass#cartcallback'
  post "cartpostback" => 'master_pass#cartpostback'
end
