# Be sure to restart your server when you modify this file.
# We are using a cache_store for our sessions in order to store more than 4k of data
MastercardMasterpassSampleApplication::Application.config.session_store :cache_store, key: '_mastercard_masterpass_sample_application_session'
