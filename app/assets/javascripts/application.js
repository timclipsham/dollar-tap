// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require jquery.payment
// require turbolinks
//= require_tree .


$(document).ready(function(){
	var amountInput = $('[name="donation[amount]"]');

	$('#tapButton').click(function(){
		var giveQueryString;
		var total = parseInt(amountInput.val(), 10);

		total++;
		amountInput.val(total);
		$("#donationAmount").text(total);
		if(total==0){
			$('.encouragement-text').text("It all starts with a tap...");
		}
		else if (total<=5){
			$('.encouragement-text').text("Thank you!");
		}
		else if (total<=10){
			$('.encouragement-text').text("Nicely done!");
		}
		else if (total<=20){
			$('.encouragement-text').text("You champion!");
		}
		else if (total<=30){
			$('.encouragement-text').text("AWESOME!");
		}

		// //Animate Coins
		// $('.coin').each(function(i){
		// 	console.log(i);

		// })
		// $('.coin').css({
		// 	'display':'block',
		// 	'left':'0',
		// 	'opacity':'1'
		// }).stop();

		// $('.coin').animate({
		// 	left: '+=100',
		// 	opacity: '0',

		// }, 300, function(){ console.log('done')});
	});

	$.get('/charities.json', function(data) {
		var charities = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('handle'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			local: data,
			limit: 10
		});

		// initialize the bloodhound suggestion engine
		charities.initialize();

		// instantiate the typeahead UI
		$('#charity-search').typeahead(null, {
			displayKey: 'handle',
			source: charities.ttAdapter()
		});
	});

	$("#search").submit(function() {
		this.action = this.action + $("#charity-search").val();
	});


	// $('[data-numeric]').payment('restrictNumeric');
	$('.cc-number').payment('formatCardNumber');
	$('.cc-exp').payment('formatCardExpiry');
	$('.cc-cvc').payment('formatCardCVC');

	// $.fn.toggleInputError = function(erred) {
		// this.parent('.form-group').toggleClass('has-error', erred);
		// return this;
	// };

	// $('form').submit(function(e) {
	// 	e.preventDefault();
	//
	// 	var cardType = $.payment.cardType($('.cc-number').val());
	// 	$('.cc-number').toggleInputError(!$.payment.validateCardNumber($('.cc-number').val()));
	// 	$('.cc-exp').toggleInputError(!$.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal')));
	// 	$('.cc-cvc').toggleInputError(!$.payment.validateCardCVC($('.cc-cvc').val(), cardType));
	// 	$('.cc-brand').text(cardType);
	//
	// 	$('.validation').removeClass('text-danger text-success');
	// 	$('.validation').addClass($('.has-error').length ? 'text-danger' : 'text-success');
	// });

	$("#tapButton").click(function() {
		$(this).addClass("zoom");
		var that = this;
		setTimeout(function() {
			$(that).removeClass("zoom");
		}, 75);
	});
});
