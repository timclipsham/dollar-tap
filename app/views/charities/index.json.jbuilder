json.array!(@charities) do |charity|
  json.extract! charity, :id, :name, :handle
  json.url charity_url(charity, format: :json)
end
