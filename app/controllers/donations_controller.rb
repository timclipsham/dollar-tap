require 'simplify'

# TODO: put in config
Simplify::public_key = "sbpb_OTJmYjFlMjgtMjI3OS00OGNiLWFjYzQtNjExYWQ1N2YzODQ5"
Simplify::private_key = "56chMAeQJ8BVAuW6SDnCgcvcrosGEV5lf/R0WiptTxJ5YFFQL0ODSXAOkNtXTToq"

class DonationsController < ApplicationController
  before_action :setup, only: [:pay, :oauthcallback]
  after_action :save_session_data, only: [:pay, :oauthcallback]

  before_action :set_donation, only: [:show, :edit, :update, :destroy]

  # GET /donations
  # GET /donations.json
  def index
    @donations = Donation.all
  end

  # GET /donations/1
  # GET /donations/1.json
  def show
  end

  # GET /donations/new
  def new
    @charity = Charity.find_by_handle(params[:handle])

    @donation = Donation.new
    @donation.charity = @charity
  end

  def pay
    @donation = Donation.new(donation_params)
    session['donation'] = @donation

    # wire up the masterpass stuff
    @data = session['data'] = MasterpassData.new("config.yml")
    @data.rewards = false
    @data.accepted_cards = "visa,master"
    @data.shipping_suppression = true
    get_request_token
    post_shopping_cart @donation.charity.name, @donation.amount.to_i
  end

  def cc
    @data = session['data']
    date = Date::parse(params['cc-exp'])
    result = charge_card format('%02d', date.month), date.year.to_s[2..-1], params['cc-number'].split(' ').join(), params['cc-cvc']

    if result
      session['donation'].save
      redirect_to "/charities/" + session['donation'].charity.handle
    else
      redirect_to "/donations/" + session['donation'].charity.handle + '/new'
    end
  end

  def oauthcallback
    handle_oauth_callback
    get_access_token
    get_checkout_data

    # puts @data.checkout.to_xml_s

    data = {
      "amount" => @data.shopping_cart.subtotal,
      "description" => @data.shopping_cart.shoppingCartItem[0].description,
      "reference" => @data.shopping_cart.shoppingCartItem[0].description,
      "card" => {
         "expMonth" => @data.checkout.card.expiryMonth,
         "expYear" => @data.checkout.card.expiryYear[2..-1],
        #  "cvc" => "123",
         "number" => @data.checkout.card.accountNumber,
         "name" => @data.checkout.card.cardHolderName
        }
    }

    # puts data

    payment = Simplify::Payment.create(data)

    if payment['paymentStatus'] == 'APPROVED'
      session['donation'].save
      redirect_to "/charities/" + session['donation'].charity.handle
    else
      redirect_to "/donations/" + session['donation'].charity.handle + '/new'
    end

    # TODO: oauthpostback (to give details to mastercard about the success/fail of transaction)
  end

  # GET /donations/1/edit
  def edit
  end

  # POST /donations
  # POST /donations.json
  def create
    @donation = Donation.new(donation_params)

    respond_to do |format|
      if @donation.save
        format.html { redirect_to @donation, notice: 'Donation was successfully created.' }
        format.json { render action: 'show', status: :created, location: @donation }
      else
        format.html { render action: 'new' }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /donations/1
  # PATCH/PUT /donations/1.json
  def update
    respond_to do |format|
      if @donation.update(donation_params)
        format.html { redirect_to @donation, notice: 'Donation was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /donations/1
  # DELETE /donations/1.json
  def destroy
    @donation.destroy
    respond_to do |format|
      format.html { redirect_to donations_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_donation
      @donation = Donation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def donation_params
      params.require(:donation).permit(:amount, :comment, :charity_id)
    end

    def setup
      # NOTE: in an actual production application, this type of session data should be stored in a
      # database. To keep our sample application database-type agnostic, we are storing this in a rails-cached session for example purposes only (note the config/initializers/session_store.rb initializer has been set to :cache_store in order to have a session larger than 4k)
      session['data'] == nil ? @data = session['data'] = MasterpassData.new("config.yml") : @data = session['data']
      @service = Mastercard::Masterpass::MasterpassService.new(@data.consumer_key, generate_private_key, @data.callback_domain, Mastercard::Common::SANDBOX)
      # create an unreferenced MasterpassDataMapper to include the mapping namespaces of our DTO's
      MasterpassDataMapper.new
      # clear any previous error messages
      @data.error_message = nil
      # get the current host name for dynamic url generation
      @data.callback_domain = @host_name = request.protocol + request.host + ":" + request.port.to_s || "http://www.givepiccolo.com"
    end

    def generate_private_key
      OpenSSL::PKCS12.new(File.open(@data.keystore_path),@data.keystore_password).key
    end

    def get_request_token
      @data.request_token_response = @service.get_request_token(@data.request_url, @data.callback_domain)
      @data.request_token = @data.request_token_response.oauth_token
      save_connection_header
    end

    def save_connection_header
      @data.auth_header = @service.auth_header
      @data.encoded_auth_header = URI.escape(@data.auth_header)
      @data.signature_base_string = @service.signature_base_string
    end

    def post_shopping_cart(charityName, amount)
      file = File.read(File.join('resources', 'shoppingCart.xml'))
      @data.shopping_cart_request = ShoppingCartRequest.from_xml(file)
      @data.shopping_cart = @data.shopping_cart_request.shoppingCart

      @data.shopping_cart.currencyCode = "AUD"
      @data.shopping_cart.subtotal = amount * 100
      @data.shopping_cart.shoppingCartItem[0].description = "Donation to " + charityName
      @data.shopping_cart.shoppingCartItem[0].value = amount * 100

      @data.shopping_cart_request.oAuthToken = @data.request_token
      @data.shopping_cart_request.originUrl = @data.callback_domain
      @data.shopping_cart_response = ShoppingCartResponse.from_xml(@service.post_shopping_cart_data(@data.shopping_cart_url, @data.shopping_cart_request.to_xml_s))
    end

    def save_session_data
      session['data'] = nil
      session['data'] = @data
    end

    def handle_oauth_callback
      @data.request_token = params['oauth_token']
      @data.verifier = params['oauth_verifier']
      @data.checkout_resource_url = params['checkout_resource_url']
      handle_pairing_callback if params['pairing_token'] && params['pairing_verifier']
    end

    def get_access_token
      @data.access_token_response = @service.get_access_token(@data.access_url, @data.request_token, @data.verifier)
      @data.access_token = @data.access_token_response.oauth_token
      save_connection_header
    end

    def get_checkout_data
      @data.checkout = Checkout.from_xml(@service.get_payment_shipping_resource(@data.checkout_resource_url, @data.access_token));
      save_connection_header
    end

    def charge_card(expMonth, expYear, number, cvc)
      data = {
        "amount" => @data.shopping_cart.subtotal,
        "description" => @data.shopping_cart.shoppingCartItem[0].description,
        "reference" => @data.shopping_cart.shoppingCartItem[0].description,
        "card" => {
           "expMonth" => expMonth,
           "expYear" => expYear,
           "number" => number
          }
      }

      data["card"]["cvc"] = cvc unless cvc.nil?

      # puts data

      payment = Simplify::Payment.create(data)
      payment['paymentStatus'] == 'APPROVED'
    end
end
