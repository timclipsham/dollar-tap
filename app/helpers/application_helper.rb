module ApplicationHelper
  def title(page_title)
    content_for(:title) {page_title}
  end 
  
  def body_class(class_name)
    content_for(:body_class) {class_name}
  end 
end
